FROM openjdk:17
COPY build/libs/Market-0.0.1-SNAPSHOT.jar /market-app/
CMD ["java", "-jar","/market-app/Market-0.0.1-SNAPSHOT.jar"]
