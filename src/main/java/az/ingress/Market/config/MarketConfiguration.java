package az.ingress.Market.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MarketConfiguration {
    @Bean
    public ModelMapper getMapper() {
        return new ModelMapper();
    }

}
