package az.ingress.Market.repo;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.entity.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketRepository extends JpaRepository<Market,Long> {


}
