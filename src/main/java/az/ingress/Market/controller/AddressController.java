package az.ingress.Market.controller;

import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.entity.Address;
import az.ingress.Market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public List<Address> getAllAddress(){
        return addressService.getAllAddress();
    }

    @GetMapping("/{id}")
    public Address getAddressById(@PathVariable Long id){
        return addressService.getAddressById(id);
    }

    @PostMapping
    public Long createAddress(@RequestBody AddressRequestDto addressRequestDto){
        return addressService.saveAddress(addressRequestDto);
    }

}
