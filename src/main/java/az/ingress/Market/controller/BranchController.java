package az.ingress.Market.controller;

import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.entity.Branch;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;

    @GetMapping
    public List<Branch> getAllBranch() {
        return branchService.getAllBranch();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BranchRequestDto> getPersonById(@PathVariable Long id) {
        BranchRequestDto branchRequestDto = branchService.getBranchById(id);
        return new ResponseEntity<>(branchRequestDto, HttpStatus.OK);
    }


    @PostMapping
    public Long createBranch(@RequestBody BranchRequestDto branchRequestDto) {
        return branchService.createBranch(branchRequestDto);
    }


}
