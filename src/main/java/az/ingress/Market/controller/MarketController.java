package az.ingress.Market.controller;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.entity.Market;
import az.ingress.Market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.error.Mark;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @GetMapping
    public List<Market> getAllMarkets() {
       return marketService.findByAll();
    }
    @GetMapping("/{id}")
    public ResponseEntity<MarketRequestDto> getMarketById(@PathVariable Long id){
        MarketRequestDto marketRequestDto = marketService.getMarketById(id);
        return new ResponseEntity<>(marketRequestDto, HttpStatus.OK);
    }

    @PostMapping
    public Long createMarket(@RequestBody @Valid MarketRequestDto marketRequestDto){
      return   marketService.createMarket(marketRequestDto);
    }

}
