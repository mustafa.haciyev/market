package az.ingress.Market.dto;

import az.ingress.Market.entity.Address;
import az.ingress.Market.entity.Market;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchRequestDto {
    Long id;
    String branchName;
    String marketId;
    String addressId;

}
