package az.ingress.Market.exception;

import az.ingress.Market.service.BranchService;

public class BranchNotFoundException extends RuntimeException{
    public BranchNotFoundException(String message){
        super(message);
    }
}
