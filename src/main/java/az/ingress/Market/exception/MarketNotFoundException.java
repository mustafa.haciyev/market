package az.ingress.Market.exception;

public class MarketNotFoundException extends RuntimeException{
    public MarketNotFoundException(String message){
        super(message);
    }
}
