package az.ingress.Market.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String branchName;

    @ManyToOne
    @JoinColumn(name = "m_id", referencedColumnName = "id")
    @JsonIgnore
    Market market;

    @OneToOne
    @JoinColumn(name = "a_id", referencedColumnName = "id")
    Address address;


}