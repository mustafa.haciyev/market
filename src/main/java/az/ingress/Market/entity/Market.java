package az.ingress.Market.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String marketName;

    @OneToMany(mappedBy = "market")
    @JsonIgnore
    List<Branch> branch;
}
