package az.ingress.Market.service;

import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.entity.Branch;

import java.util.List;

public interface BranchService {
    Long createBranch(BranchRequestDto branchRequestDto);



    BranchRequestDto getBranchById(Long id);

    List<Branch> getAllBranch();

}
