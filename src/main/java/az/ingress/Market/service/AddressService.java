package az.ingress.Market.service;

import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.entity.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAllAddress();

    Address getAddressById(Long id);

    Long saveAddress(AddressRequestDto addressRequestDto);
}
