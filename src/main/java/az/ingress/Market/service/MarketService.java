package az.ingress.Market.service;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.entity.Market;

import java.util.List;

public interface MarketService {
    Long createMarket(MarketRequestDto marketRequestDto);

    MarketRequestDto getMarketById(Long id);



    List<Market> findByAll();
}
