package az.ingress.Market.service.impl;

import az.ingress.Market.config.MarketConfiguration;
import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.entity.Address;
import az.ingress.Market.repo.AddressRepository;
import az.ingress.Market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressServiceİmpl implements AddressService {

    private final AddressRepository addressRepository;
    private final MarketConfiguration marketConfiguration;

    @Override
    public List<Address> getAllAddress() {
        return addressRepository.findAll();
    }

    @Override
    public Address getAddressById(Long id) {
        return addressRepository.findById(id).get();
    }

    @Override
    public Long saveAddress(AddressRequestDto addressRequestDto) {
        Address address = marketConfiguration.getMapper().map(addressRequestDto, Address.class);
        return addressRepository.save(address).getId();
    }
}
