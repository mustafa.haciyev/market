package az.ingress.Market.service.impl;

import az.ingress.Market.config.MarketConfiguration;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.entity.Market;
import az.ingress.Market.exception.MarketNotFoundException;
import az.ingress.Market.repo.MarketRepository;
import az.ingress.Market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final MarketConfiguration marketConfiguration;



    @Override
    public List<Market> findByAll() {
        return marketRepository.findAll();
    }

    @Override
    public MarketRequestDto getMarketById(Long id) {
        Market market = marketRepository.findById(id)
                .orElseThrow(() -> new MarketNotFoundException("Branch not found with id: " + id));
        MarketRequestDto marketRequestDto = marketConfiguration.getMapper().map(market,MarketRequestDto.class);
        return marketRequestDto;
    }


    @Override
    public Long createMarket(MarketRequestDto marketRequestDto) {
        Market market = marketConfiguration.getMapper().map(marketRequestDto, Market.class);
        return marketRepository.save(market).getId();
    }




}
