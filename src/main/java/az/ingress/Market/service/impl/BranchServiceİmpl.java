package az.ingress.Market.service.impl;

import az.ingress.Market.config.MarketConfiguration;
import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.entity.Branch;
import az.ingress.Market.exception.BranchNotFoundException;
import az.ingress.Market.repo.AddressRepository;
import az.ingress.Market.repo.BranchRepository;
import az.ingress.Market.repo.MarketRepository;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceİmpl implements BranchService{
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final AddressRepository addressRepository;
    private final MarketConfiguration marketConfiguration;






    @Override
    public List<Branch> getAllBranch() {
        return branchRepository.findAll();
    }

    @Override
    public BranchRequestDto getBranchById(Long id) {
        Branch branch = branchRepository.findById(id)
                .orElseThrow(() -> new BranchNotFoundException("Branch not found with id: " + id));

        BranchRequestDto branchRequestDto = marketConfiguration.getMapper().map(branch, BranchRequestDto.class);
        branchRequestDto.setMarketId(branch.getMarket().getMarketName());
        branchRequestDto.setAddressId(branch.getAddress().getAddressName());

        return branchRequestDto;
    }


    @Override
    public Long createBranch(BranchRequestDto branchRequestDto) {

        Branch branch = marketConfiguration.getMapper().map(branchRequestDto, Branch.class);

        return branchRepository.save(branch).getId();
    }


}
